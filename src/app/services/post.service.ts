import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { Post } from '../models/post.model';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  private posts: Post[] = [
    {
      id: 1,
      title:  'Mon premier Post',
      content: 'Voici mon premier post dans le blog',
      likeIts: 0,
      disLikeIts: 0,
      createdAt: new Date('1968-11-19T04:12:00')
    },
    {
      id: 2,
      title: 'Mon deuxième Post',
      content: 'Voici mon deuxième post dans le blog',
      likeIts: 0,
      disLikeIts: 0,
      createdAt: new Date('2018-11-20T22:56:00')
    },
    {
      id: 3,
      title: 'Encore un Post',
      content: 'Voici mon troisème post dans le blog',
      likeIts: 0,
      disLikeIts: 0,
      createdAt: new Date('2018-11-22T13:00:00')
    }
];

  postSubject = new Subject<Post[]>();


  emitPostSubject() {
    this.postSubject.next(this.posts.slice());
  }

  getPostById(id: number) {
    const post = this.posts.find(
        (postObject) => {
            return postObject.id === id;
        }
    );
    return post;
}

getPostIndexById(id: number) {
  const postIndex = this.posts.findIndex(
    (postObject) => {
      return postObject.id === id;
    }
  );
  return postIndex;
}

likeOnePost(id: number) {
  this.getPostById(id).likeIts++;
  this.emitPostSubject();
}

dislikeOnePost(id: number) {
  this.getPostById(id).disLikeIts--;
  this.emitPostSubject();
}

deleteOnePost(id: number) {
  const index = this.getPostIndexById(id);
  this.posts.splice(index, 1);
  this.emitPostSubject();
}

addPost(post: Post) {
  this.posts.push(post);
  this.emitPostSubject();
}




getMaxPostId() {
  if (this.posts.length > 0) {
    return this.posts.reduce(function(prev, current) {
      return (prev.id > current.id) ? prev : current; });
}
}

getNewPostId() {
  if (this.posts.length > 0) {
  return this.getMaxPostId().id + 1;
} else {
  return 1;
}
}

sortPosts(likeOrDislike) {
  this.posts = this.posts.sort(function(obj1, obj2) {
    if (likeOrDislike === 'likeIts') {
    return obj2.likeIts - obj1.likeIts;
  } else {
    return  obj1.disLikeIts - obj2.disLikeIts;
  }
  });
  this.emitPostSubject();

}

}
