export class Post {

    constructor (public title: string,
        public content: string,
        public id: number,
        public likeIts: number,
        public disLikeIts: number,
        public createdAt: Date) {
        }
}
