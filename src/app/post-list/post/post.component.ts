import { Component, Input, OnInit } from '@angular/core';
import {PostService } from '../../services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input() postTitle: string;
  @Input() id: number;
  @Input() postContent: string;
  @Input() postLikeIts: number;
  @Input() postCreatedAt: Date;
  @Input() postDislikeIts: number;



  constructor(private postService: PostService) { }

  ngOnInit() {
  }
  onLike() {
    this.postService.likeOnePost(this.id);
  }

  onDislike() {
    this.postService.dislikeOnePost(this.id);
  }

  onDelete() {
    this.postService.deleteOnePost(this.id);
  }


}
