import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post.model';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  postForm: FormGroup;



  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private postService: PostService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
  this.postForm = this.formBuilder.group({
    title: ['', Validators.required],
    content: ['', Validators.required],
    });
  }

  onSubmitForm() {
    const formValue = this.postForm.value;
    const newPost = new Post (
      formValue['title'],
      formValue['content'],
      this.postService.getNewPostId(),
      0,
      0,
      new Date(),
    );
    this.postService.addPost(newPost);
    this.router.navigate(['posts']);

}

}
